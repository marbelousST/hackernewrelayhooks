import React, { useState } from 'react';
import CreateLinkMutation from '../mutations/CreateLinkMutation';
import { GC_USER_ID } from '../constants';

const CreateLink = props => {
  const [description, setDescription] = useState('');
  const [url, setUrl] = useState('');

  const _createLink = () => {
    const postedById = localStorage.getItem(GC_USER_ID);
    if (!postedById) {
      console.error('No user logged in');
      return;
    }
    CreateLinkMutation(postedById, description, url, () =>
      props.history.push('/')
    );
  };

  return (
    <div>
      <div className='flex flex-column mt3'>
        <input
          className='mb2'
          value={description}
          onChange={e => setDescription(e.target.value)}
          type='text'
          placeholder='A description for the link'
        />
        <input
          className='mb2'
          value={url}
          onChange={e => setUrl(e.target.value)}
          type='text'
          placeholder='The URL for the link'
        />
      </div>
      <div className='button' onClick={() => _createLink()}>
        submit
      </div>
    </div>
  );
};

export default CreateLink;
