import React, { useState } from 'react';
import { GC_USER_ID, GC_AUTH_TOKEN } from '../constants';
import SigninMutation from '../mutations/AuthenticateUserMutation';
import CreateUserMutation from '../mutations/SignupUserMutation';

const Login = props => {
  const [login, setLogin] = useState(true);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');

  const _confirm = () => {
    if (login) {
      SigninMutation(email, password, (id, token) => {
        _saveUserData(id, token);
        props.history.push(`/`);
      });
    } else {
      CreateUserMutation(name, email, password, (id, token) => {
        _saveUserData(id, token);
        props.history.push(`/`);
      });
    }
  };

  const _saveUserData = (id, token) => {
    localStorage.setItem(GC_USER_ID, id);
    localStorage.setItem(GC_AUTH_TOKEN, token);
  };
  return (
    <div>
      <h4 className='mv3'>{login ? 'Login' : 'Sign Up'}</h4>
      <div className='flex flex-column'>
        {!login && (
          <input
            value={name}
            onChange={e => setName(e.target.value)}
            type='text'
            placeholder='Your name'
          />
        )}
        <input
          value={email}
          onChange={e => setEmail(e.target.value)}
          type='text'
          placeholder='Your email address'
        />
        <input
          value={password}
          onChange={e => setPassword(e.target.value)}
          type='password'
          placeholder='Choose a safe password'
        />
      </div>
      <div className='flex mt3'>
        <div className='pointer mr2 button' onClick={() => _confirm()}>
          {login ? 'login' : 'create Account'}
        </div>
        <div className='pointer button' onClick={() => setLogin(!login)}>
          {this.state.login
            ? 'need to create an account?'
            : 'already have an account?'}
        </div>
      </div>
    </div>
  );
};

export default Login;
